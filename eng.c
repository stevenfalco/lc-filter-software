// Modified to use a caller-supplied buffer.  SAF 2023-09-04

/* Print a floating-point number in engineering notation */
/* Documentation: http://www.cs.tut.fi/~jkorpela/c/eng.html */

#define PREFIX_START (-24)
/* Smallest power of ten for which there is a prefix defined.
   If the set of prefixes will be extended, change this constant
   and update the table "prefix". */

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "eng.h"

void
eng(char *buf, int bufSize, double value, int digits, int numeric)
{
	static char *prefix[] = {
		"y", "z", "a", "f", "p", "n", "u", "m", "",
		"k", "M", "G", "T", "P", "E", "Z", "Y"
	}; 
#define PREFIX_END (PREFIX_START + (int)((sizeof(prefix) / sizeof(char *) - 1) * 3))

	int expof10;

	if(value < 0.0) {
		*buf++ = '-';
		bufSize--;
		value = -value;
	}

	if(value == 0.0) {
		strncpy(buf, "0.0", bufSize);
		return;
	}

	expof10 = (int)log10(value);

	if(expof10 > 0) {
		expof10 = (expof10 / 3) * 3;
	} else {
		expof10 = (-expof10 + 3) / 3 * (-3); 
	}

	value *= pow(10, -expof10);

	if(value >= 1000.0) {
		value /= 1000.0;
		expof10 += 3;
	} else if(value >= 100.0) {
		digits -= 2;
	} else if(value >= 10.0) {
		digits -= 1;
	}

	if(numeric || (expof10 < PREFIX_START) || (expof10 > PREFIX_END)) {
		snprintf(buf, bufSize, "%.*fe%d", digits - 1, value, expof10); 
	} else {
		snprintf(buf, bufSize, "%.*f %s", digits - 1, value, prefix[(expof10 - PREFIX_START) / 3]);
	}
}
