// This is a translation of the symmetrical Fortran code appearing on page
// 1009 of the IEEE Transactions on Circuits and Systems, Vol. CAS-25, No. 12
// of December 1978 by Pierre Amstutz.
//
// This translation (C) 2023 Steven A. Falco.
#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <string.h>
#include <unistd.h>

#define BS	512
#define SBS	32

#include "eng.h"

int
main()
{
	double B[16 + 1], C[16 + 1], D[16 + 1], E[15 + 1], F[30 + 1], W, X, Y, Z;
	double fStop, fPass, fCenter;
       	double P, Q, R, S;
	double Qcritical, ripple, freq3dB;

	double dbToNepers = log(10.0) / 10.0;

	int J, K, L, M, N;

	char ibuf[BS], obufC[SBS], obufF[SBS], obufD[SBS], obufB[SBS];

	printf("*:*:* SYMMETRICAL ELLIPTIC FILTERS *:*:*\n");

	// Line 2000 //////////////////////////////////////////////////////////

	while(1) {
		printf("STOPBAND EDGE (HZ) ? ");
		fgets(ibuf, BS, stdin);
		fStop = atof(ibuf) / 1000.0;

		printf("PASSBAND EDGE (HZ) ? ");
		fgets(ibuf, BS, stdin);
		fPass = atof(ibuf) / 1000.0;

		if(fabs(fStop - fPass) <= 0) {
			printf("Stopband and passband cannot be equal\n");
			continue;
		}

		while(1) {
			printf("NUMBER OF PEAKS (1-15) ? ");
			fgets(ibuf, BS, stdin);
			N = atoi(ibuf);
			if(N <= 0 || N > 15) {
				printf("Number of peaks must be between 1 and 15\n");
				continue;
			}
			M = (2 * N) + 1;

			// Find exp(-2 * u) which is in variable Y.  Also,
			// estimate the critical Q value (variable Qcritical).

			// Line 2080
			fCenter = sqrt(fStop * fPass);
			R = fCenter + fCenter;

			// Line 2100
			for(K = 1; K <= 2; K++) {
				S = fStop + fPass;
				for(J = 1; J <= 6; J++) {
					P = sqrt(S * R);
					S = (S + R) / 2;

					if(1e8 * (S - P) < S) {
						break;
					}

					R = P;
				}

				if(K >= 2) {
					break;
				}

				Q = M / S;
				R = fabs(fStop - fPass);
			}

			// Line 2200
			Q = Q * S;
			S = exp(-M_PI / Q);
			Y = S;
			Qcritical = Q / (4 * (1 - S) * pow(S, N));

			// Find "a sub p" and tan(w), where tan(w) is in
			// variable W, using u and "a sub s".  Also find
			// the 3 dB frequency in variable freq3dB.

			// Line 2250
			printf("STOPBAND REJECTION (DB) ? ");
			fgets(ibuf, BS, stdin);
			S = atof(ibuf);

			// Line 2260
			if(S <= 0) {
				printf("Stopband rejection must be a positive number\n");
				continue;
			}

			// Line 2270
			S = exp(S * dbToNepers / 2);
			R = exp(M_PI * Q);
			P = log(1 + ((S * S - 1) / pow((R / 4) + (1 / R), 2))) / dbToNepers;
			ripple = P;

			// Line 2310
			R = R / (2 * (S + sqrt(S * S - 1)));
			R = log(R + sqrt(R * R + 1)) / (2 * Q);
			R = sin(R) / cos(R);
			W = R;
			freq3dB = 1000.0 * (fPass + (fStop - fPass) / (1 + fCenter / (fPass * R * R)));

			// Line 2360
			printf("NOMINAL RESISTANCE (OHM) ? ");
			fgets(ibuf, BS, stdin);
			R = atof(ibuf);
			if(R > 0) {
				break;
			} else {
				printf("Resistance must be greater than zero\n");
				continue;
			}
		}

		eng(obufF, SBS, freq3dB, 4, 0);
		printf("\n");
		printf("CRITICAL Q      = %.4g\n", Qcritical);
		printf("PASSBAND RIPPLE = %.4g dB\n", ripple);
		printf("3 DB POINT      = %sHz\n", obufF);

		// Find "Omega sub j" (variable F[j]) and
		// "a sub 0" (variable E[N]).

		// Line 2390
		Z = Y;
		E[N] = W;
		W = W * W;

		// Line 2400
		for(J = 1; J <= (M - 1); J++) {
			F[J] = 1;
		}
		K = 1;

		// Line 2430
		for(J = 1; J <= 1024; J++) {
			F[K] = F[K] * (1 - Z) / (1 + Z);
			if(K >= M - 1) {
				Z = Z * Y;
				X = pow((1 - Z) / (1 + Z), 2);
				E[N] = E[N] * (W + X) / (1 + W * X);
				K = 0;
			}
			Z = Z * Y;

			if(Z < 0.25e-18) {
				break;
			}

			K = K + 1;
		}

		// Line 2530
		for(J = 1; J <= N; J++) {
			F[J] = F[J] * F[M - J];
			F[M - J] = F[J];
		}

		// Line 3000 //////////////////////////////////////////////////////////

		// Find the starting values for the permutation method described in
		// section III-B of the original paper.  Also, take section V-C1
		// into account.

		// Line 3010
		for(J = 1; J <= N; J++) {
			D[J] = F[2 * J] * (1 - pow(F[J], 4)) / F[J];
			B[J] = E[N] * F[J];
		}

		// Line 3040
		C[1] = 1.0 / B[N];

		// Line 3050
		for(J = 1; J <= (N - 1); J++) {
			C[J + 1] = (C[J] - B[N - J]) / (1 + C[J] * B[N - J]);
			E[N - J] = E[N + 1 - J] + E[N] * D[J] / (1 + B[J] * B[J]);
		}

		// Line 4000 //////////////////////////////////////////////////////////

		// Line 4010
		for(J = 1; J <= N; J++) {
			B[J] = ((1 + C[J] * C[J]) * E[J] / D[J] - C[J] / F[J]) / 2;
			C[J] = C[J] * F[J];
			D[J] = F[J] * F[J];
		}

		// Line 4050
		B[N + 1] = B[N];
		C[N + 1] = C[N];
		D[N + 1] = D[N];

		// Line 5000 //////////////////////////////////////////////////////////

		// Perform the permutation.

		// Line 5010
		if(N != 1) {
			for(L = 1; L <= 2; L++) {
				for(K = L + 2; K <= N + 1; K += 2) {
					for(J = L; J <= K - 2; J += 2) {
						Y = C[J] - C[K];
						Z = 1 / (Y / (B[J] * (D[K] - D[J])) - 1);
						B[K] = (B[K] - B[J]) * Z * Z - B[J] * (1 + Z + Z);
						C[K] = Y * Z;
					}
				}
			}

			// Line 6000 //////////////////////////////////////////////////

			// Begin denormalizing the results.

			// Line 6010
			S = B[N] / B[N + 1] - 1;
		}

		// Line 6020
		Q = 0.0005 /(M_PI * fCenter);

		// Line 6030
		P = Q * R;
		Q =Q / R;

		// Line 6040
		if(fStop < fPass) {
			// Line 6150
			printf("\n*:* HIGH-PASS FILTER *:*\n\n");
			for(J = 1; J <= N; J++) {
				C[J] = Q / C[J];
				D[J] = Q / (B[J] * D[J]);
				B[J] = P * B[J];
				F[J] = fCenter * F[J];
			}
			C[N + 1] = Q / C[N + 1];
		} else {
			// Line 6060
			printf("\n*:* LOW-PASS FILTER *:*\n\n");
			for(J = 1; J <= N; J++) {
				C[J] = Q * C[J];
				D[J] = Q * B[J] * D[J];
				B[J] = P / B[J];
				F[J] = fCenter / F[J];
			}
			C[N + 1] = Q * C[N + 1];
		}

		// Line 6240
		for(J = 1; J <= N; J += 2) {
			eng(obufC, SBS, C[J], 4, 0);
			eng(obufF, SBS, F[J] * 1000.0, 4, 0);
			eng(obufD, SBS, D[J], 4, 0);
			eng(obufB, SBS, B[J], 4, 0);
			printf("%sF\n", obufC);
			printf("%sF %sH (%sHz)\n", obufD, obufB, obufF);
		}

		// Line 6270
		eng(obufC, SBS, C[N + 1], 4, 0);
		printf("%sF\n", obufC);

		// Line 6280
		if(N != 1) {
			L = ((N + 1) / 2) * 2;
			K = M - 1 - L;

			// Line 6300
			for(J = L + 2; J <= M - 1; J += 2) {
				eng(obufC, SBS, C[K], 4, 0);
				eng(obufF, SBS, F[K] * 1000.0, 4, 0);
				eng(obufD, SBS, D[K], 4, 0);
				eng(obufB, SBS, B[K], 4, 0);
				printf("%sF %sH (%sHz)\n", obufD, obufB, obufF);
				printf("%sF\n", obufC);
				K = K - 2;
			}

			// Line 6340
			//
			// The precision test shows the discrepancy between
			// the values of the central inductor as calculated
			// from both ends of the filter.
			printf("\nPRECISION TEST: %#e\n", S);
		}

		exit(0);
	}
}
