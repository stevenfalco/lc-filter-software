all: symmetric antimetric

symmetric: symmetric.c eng.c
	cc -Wall -Werror -o $@ $^ -lm

antimetric: antimetric.c eng.c
	cc -Wall -Werror -o $@ $^ -lm
