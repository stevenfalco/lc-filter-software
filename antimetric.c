// This is a translation of the antimetrical Fortran code appearing on page
// 1008 of the IEEE Transactions on Circuits and Systems, Vol. CAS-25, No. 12
// of December 1978 by Pierre Amstutz.
//
// This translation (C) 2023 Steven A. Falco.
#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#define BS	512
#define SBS	32

#include "eng.h"

int
main()
{
	char T;

	double U, V, W, X, Y, Z, A0, E0, E8, FP, FS, SR, TQ, T0;
	double B[16 + 1], C[16 + 1], D[16 + 1], E[30 + 1], F[16 + 1];
       	double R[15], S[15], DB[16 + 1], TB[16 + 1];
	double AS, EAS, AP, EAP;

	int I, J, K, L, M, N, IT;

	double dbToNepers = log(10.0) / 10.0;

	char ibuf[BS];
	char obuf1[SBS];
	char obuf2[SBS];
	char obufP[SBS];

	printf("** ANTIMETRICAL ELLIPTIC FILTERS * TYPES A,B AND C **\n");

	while(1) {
		// Line 1060 - 1070
		printf("REJECTION ? ");
		fgets(ibuf, BS, stdin);
		AS = atof(ibuf);

		printf("RIPPLE(DB) ? ");
		fgets(ibuf, BS, stdin);
		AP = atof(ibuf);

		printf("HALF-DEGREE (2-15) ? ");
		fgets(ibuf, BS, stdin);
		M = atoi(ibuf);

		printf("TYPE (A, B OR C) ? ");
		fgets(ibuf, BS, stdin);
		T = tolower(ibuf[0]);

		// Line 1080
		if(AS <= AP) {
			printf("Rejection must be greater than ripple\n");
			continue;
		}

		if(M < 2 || M > 15) {
			printf("Half degree must be between 2 and 15\n");
			continue;
		}

		if(T != 'a' && T != 'b' && T != 'c') {
			printf("Type must be A, B, or C\n");
			continue;
		}

		break;
	}

	N = 2 * M;

	// Line 2000 //////////////////////////////////////////////////////////

	// Find the frequencies (E[J], and parameters U and A0 for a true
	// elliptic filter of degree M, using the stopband rejection AS
	// and the passband ripple AP.
	//
	// Frequencies are normalized such that EP * ES = 1.

	// Line 2010
	EAS = exp(dbToNepers * AS) - 1.0;
	EAP = exp(dbToNepers * AP) - 1.0;

	// Line 2030
	V = sqrt(EAS / EAP) + sqrt(EAS / EAP - 1.0);
	U = M_PI * M_PI / (2.0 * log(V + V));
	V = V / (sqrt(EAS) + sqrt(EAS + 1.0));
	W = U * log(V + sqrt(V * V - 1.0)) / M_PI;
	W = sin(W) / cos(W);
	A0 = W;
	W = W * W;
	Y = exp(-U);
	Z = Y;
	K = M - 1;

	// Line 2090
	for(J = 1; J <= N; J++) {
		E[J] = 1;
	}

	// Line 2110
	for(J = 1; J <= 1024; J++) {
		if(K == M) {
			X = pow((1.0 - Z) / (1.0 + Z), 2.0);
			A0 = A0 * (W + X) / (1.0 + W * X);
		}

		E[K] = E[K] * (1.0 - Z) / (1.0 + Z);
		Z = Z * Y;

		if(Z < 0.25e-18) {
			break;
		}

		K = K - 1;
		if(K == 0) {
			K = N;
		}
	}

	// Line 2180
	E[M] = 0;
	E[N] = E[N] * E[N];

	printf("True elliptic filter:\n");
	printf(" U = %.16e      A0 = %.16e\n", U, A0);
	printf(" EP= %.16e\n", E[N]);

	// Line 2200
	for(J = 1; J <= M - 1; J++) {
		E[J] = -E[J] * E[N-J];
		printf(" E = %.16e\n", -E[J]);
		E[N - J] = -E[J];
	}

	// Line 2240 //////////////////////////////////////////////////////////

	// Separate the E[J] frequencies into a real and imaginary part
	// named R[J] and S[j] respectively.

	// Line 2250
	X = sqrt(A0 * A0 + 1.0 / (A0 * A0) + E[N] * E[N] + 1.0 / (E[N] * E[N]));

	printf("\nNatural frequencies:\n");

	// Line 2260
	for(J = 1; J <= M - 1; J += 2) {
		K = (J + 1) / 2;
		Y = A0 * E[J];
		Y = Y + 1.0 / Y;
		Z = E[N] * E[J];
		R[K] = E[M - J] * (1.0 / Z - Z) / Y;
		S[K] = -X / Y;

		// Line 2300
		printf(" RE = %.16e    SE = %.16e j\n", R[K], S[K]);
		R[M - K + 1] = R[K];
		S[M - K + 1]= -S[K];
	}

	// Line 2330
	if(K + K != M) {
		R[K + 1] = -A0;
		S[K + 1] = 0;
		printf(" RE = %.16e\n", -A0);
	}

	// Line 3000 //////////////////////////////////////////////////////////

	// Calculate the parameters of derived filters of type A, B, or C
	// where the degree N is 2 * M.  M is the number of half-peaks.  See
	// section IV-F of the original paper.
	//

	// Line 3010
	IT = 2;
	if(T == 'a') {
		IT = 1;
	}

	// Line 3020
	E8 = -E[1];
	if(T == 'a') {
		E8 = E[N];
	}

	// Line 3030
	E0 = E[N];
	if(T == 'c') {
		E0 = -E[1];
	}

	// Line 3040
	FP = sqrt((E[N] + E0) / (1.0 + E[N] * E8));
	FS = sqrt((1.0 + E[N] * E0) / (E[N] + E8));

	// Line 3060
	D[1]=0;
	for(J = IT; J <= M; J++) {
		D[J] = (E[2 * J - 1] + E8) / (1.0 + E[2 * J - 1] * E0);
		F[J] = sqrt(1.0 / D[J]);
	}

	// Line 3090 //////////////////////////////////////////////////////////

	// Line 3100
	SR = 0;
	TQ = 0;
	T0 = 0;
	B[1] = 0;
	I = 1;

	printf("\nDerived filter natural frequencies:\n");

	// Line 3110
	for(J = 1; J <= M; J++) {
		W = (pow(A0, 2.0) + pow(E[2 * J - 1], 2.0)) / (1.0 + pow(A0 *E[2 * J - 1], 2.0));
		X = (1.0 + E0 * E8) * S[J] + E0 + E8 * W;
		Y = pow(E0, 2.0) + 2 * E0 * S[J] + W;
		Z = 1.0 + 2 * E8 * S[J] + pow(E8, 2.0) * W;
		U= sqrt(Y / Z);
		V = X / Z;
		R[J] = sqrt((U - V) / 2.0);
		S[J] = sqrt((U + V) / 2.0);
		printf(" RF = %.16e    SF = %.16e j\n", -R[J] / FP, S[J] / FP);

		// Line 3200
		SR = SR + R[J] / U;
		I = -I;
		W = I * R[J] / S[J];
		TQ = (TQ + W) / (1.0 - TQ * W);

		// Line 3230
		if(T != 'a') {
			B[1] = B[1] + R[J];
			continue;
		}

		// Line 3240
		U = (F[2] - S[J]) / R[J];
		V = (F[2] + S[J]) / R[J];
		W = I * (V - U) / (1.0 + U * V);
		T0 = (T0 + W) / (1.0 - T0 * W);
		B[1] = B[1] + R[J];
	}

	// Line 4000 //////////////////////////////////////////////////////////

	// Line 4010
	if(T == 'a') {
		T0 = T0 / (1.0 + sqrt(1.0 + T0 * T0));
	}

	// Line 4020
	for(K = IT; K <= M; K++) {
		DB[K] = 0;
		TB[K] = T0;
		I = 1;

		// Line 4040
		for(J = 1; J <= M; J++) {
			DB[K] +=  1.0 / (R[J] + pow(F[K] - S[J], 2.0) / R[J])
				+ 1.0 / (R[J] + pow(F[K] + S[J], 2.0) / R[J]);
			I = -I;
			W = (F[K] - I * S[J]) / R[J];
			TB[K] = (TB[K] + W) / (1.0 -TB[K] * W);
		}
	}

	// Line 5000 //////////////////////////////////////////////////////////

	// Find "L sub j1" (called C[J] below) and "B sub j1" (called B[J])
	// for the permutation method described in section III-B of the
	// original paper.
	//
	// This uses TB[J] and the derivative of the phase DB[J] at the
	// peak frequency F[J].

	// Line 5010
	D[M + 1] = D[M];
	F[M + 1] = F[M];
	DB[M + 1] = DB[M];
	TB[M + 1] = TB[M];
	C[1] = 0;

	// Line 5020
	for(J = 1; J <= M + 1 - IT; J += 2) {
		TB[M + 1 - J] = -1.0 / TB[M + 1 - J];
	}

	// Line 5040
	for(J = IT; J <= M + 1; J++) {
		B[J] = (1.0 + pow(TB[J], 2.0)) * DB[J] / (4.0 * D[J]) - TB[J] * F[J] / 2.0;
		C[J] = TB[J] / F[J];
	}

	// Line 6000 //////////////////////////////////////////////////////////

	// Calculate the element values as per section III-B2.

	// Line 6010
	for(L = 1; L <= 2; L++) {
		for(K = L + 2; K <= M + 1; K += 2) {
			for(J = L; J <= K - 2; J += 2) {
				U = C[J] - C[K];
				V = 1.0 / (U / (B[J] * (D[K] - D[J])) - 1.0);
				B[K] = (B[K] - B[J]) * V * V - B[J] * (V + V + 1);
				C[K] = U * V;
			}
		}
	}

	// Line 7000 //////////////////////////////////////////////////////////

	// Find the load resistance, using equations 3.15 and 3.16.

	// Line 7010
	W = 1.0;
	if(T != 'c') {
		W = pow((1.0 - TQ * T0) / (TQ + T0), 2.0);
	}

	// Line 7020
	for(J = 1; J <= M + 1; J += 2) {
		B[J] = B[J] * W;
		C[J] = C[J] * W;
	}

	// Line 7040
	printf("\nLOAD RESISTANCE = %g, Inverse = %g\n\n", W, 1.0 / W);

	// Line 7050
	printf("             L  ( C )           C  ( L )               PEAK\n");
	if(T != 'a') {
		eng(obuf2, SBS, FP / B[1], 8, 0);
		printf("1                      %20s\n", obuf2);
	}

	// Line 7090
	V = 0;
	for(J = IT; J <= M; J++) {
		V = V + C[J];
		eng(obuf1, SBS, FP * C[J], 8, 0);
		eng(obuf2, SBS, FP / B[J], 8, 0);
		eng(obufP, SBS, FS / FP, 8, 0);
		printf("%d %20s %20s %20s\n", J, obuf1, obuf2, obufP);
	}

	// Line 7110
	eng(obuf1, SBS, FP * C[M + 1], 8, 0);
	printf("%d %20s\n\n", M + 1, obuf1);
	printf("STOPBAND EDGE = %g\n\n", FS / FP);

	// The first test shows the discrepancy between the central capacitor
	// value as calculated from each end of the filter.  Ideally, we would
	// get the same value either way.
	//
	// The second test shows the difference between the sum of all the
	// series inductor values and the estimate that was made earlier using
	// equation 3.8.
	printf("TESTS %g %g\n", B[M] / B[M + 1] - 1, (W + 1) * SR - V - C[M + 1]);
}
