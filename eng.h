#ifndef __ENG_FORMATTER__
#define __ENG_FORMATTER__

void eng(char *buf, int bufSize, double value, int digits, int numeric);

#endif // __ENG_FORMATTER__
